<?php
namespace BitbucketApi\Model;

/**
 * Interface des Items
 *
 * @package Bitbucket API
 * @copyright Addweb 2013
 * @author Romain QUILLIOT <romain.addweb@gmail.com>
 */
interface Item
{    
    public function __construct($replace, $infos);
    public function get(Array $params);
    public function post(Array $params);
    public function put(Array $params);
    public function delete(Array $params);
}
