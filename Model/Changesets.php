<?php
namespace BitbucketApi\Model;

use BitbucketApi\Controller\Curl;

/**
 * Gestion des Changesets
 *
 * @package Bitbucket API
 * @copyright Addweb 2013
 * @author Romain QUILLIOT <romain.addweb@gmail.com>
 */
class Changesets implements Item
{
    private $pattern = "https://bitbucket.org/api/1.0/repositories/{username}/{repository}/changesets/";
    
    /**
     * Permet de créer le pattern et initialise cURL
     *
     * @param array $replace
     * @param string $infos
     */
    public function __construct($replace, $infos)
    {
        $this->pattern = str_replace(['{username}','{repository}'], [$replace['account'], $replace['repository']], $this->pattern);
        CURL::init();
        CURL::setID($infos);
    }
    
    /**
     * Recupere les données des changements
     * 
     * <code>
     * $client->Changesets('xpressimport', 'romainjeff')->get([
     *     'node' => 'dev', 
     *     'stats' => true
     * ])
     * </code>
     *
     * @param array $params
     * [] = Tous les changements <br>
     * ['node'] = Le nom d'une branche ou d'un commit <br>
     * ['stats] = Recupère les statistiques
     * @return array
     */
    public function get(Array $params)
    {
        if (!empty($params['node'])) {
            if (!empty($params['stats'])) {
                CURL::setUrl($this->pattern . $params['node'] ."/diffstat/");
            } else {
                CURL::setUrl($this->pattern . $params['node']);
            }
        } else {
            CURL::setUrl($this->pattern);
        }
        
        CURL::execute();
        CURL::close();
        
        $result = json_decode(CURL::result(), 1);
        
        return $result;
    }
    
    public function post(Array $params){}
    public function put(Array $params){}
    public function delete(Array $params){}
}
