<?php
namespace BitbucketApi\Model;

use BitbucketApi\Controller\Curl;

/**
 * Gestion des Branches
 *
 * @package Bitbucket API
 * @copyright Addweb 2013
 * @author Romain QUILLIOT <romain.addweb@gmail.com>
 */
class Branches implements Item
{
    private $pattern = "https://bitbucket.org/api/1.0/repositories/{username}/{repository}/";
    
    /**
     * Permet de créer le pattern et initialise cURL
     *
     * @param array $replace
     * @param string $infos
     */
    public function __construct($replace, $infos)
    {
        $this->pattern = str_replace(['{username}','{repository}'], [$replace['account'], $replace['repository']], $this->pattern);
        CURL::init();
        CURL::setID($infos);
    }
    
    /**
     * Recupere les données des branches
     * 
     * <code>
     * $client->Branches('xpressimport', 'romainjeff')->get([
     *     'main' => true
     * ])
     * </code>
     *
     * @param array $params
     * [] = Toutes les branches <br>
     * ['main'] = La branche principale
     */
    public function get(Array $params)
    {
        if (!empty($params['main'])) {
            CURL::setUrl($this->pattern . "main-branch/");
        } else {
            CURL::setUrl($this->pattern . "branches/");
        }
        
        CURL::execute();
        CURL::close();
        
        $result = json_decode(CURL::result(), 1);
        
        return $result;
    }
    
    public function post(Array $params){}
    public function put(Array $params){}
    public function delete(Array $params){}
}