<?php
namespace BitbucketApi\Model;

use BitbucketApi\Controller\Curl;

/**
 * Gestion des Issues
 *
 * @package Bitbucket API
 * @copyright Addweb 2013
 * @author Romain QUILLIOT <romain.addweb@gmail.com>
 */
class Issues implements Item
{
    private $pattern = "https://bitbucket.org/api/1.0/repositories/{username}/{repository}/issues/";
    
    /**
     * Permet de créer le pattern et initialise cURL
     *
     * @param array $replace
     * @param string $infos
     */
    public function __construct($replace, $infos)
    {
        $this->pattern = str_replace(['{username}','{repository}'], [$replace['account'], $replace['repository']], $this->pattern);
        CURL::init();
        CURL::setID($infos);
    }
    
    /**
     * Permet de recuperer une entrée
     *
     * <code>
     * $client->Issues('bitbucket-client-api', 'romainjeff')->get([
     *     'id' => 3
     * ])
     * </code>
     *
     * @param array $params
     * ['id'] = indique l'id de l'issue que l'on souhaite<br>
     * [] = récupère toutes les issues
     * @return string
     */
    public function get(Array $params)
    {
        if (!empty($params['id'])) {
            CURL::setUrl($this->pattern . $params['id']);
        } else {
            CURL::setUrl($this->pattern);
        }
        
        CURL::execute();
        CURL::close();
        
        $result = json_decode(CURL::result(), 1);
        
        return $result;
    }
    
    /**
     * Permet d'ajouter une entrée
     *
     * <code>
     * $client->Issues('bitbucket-client-api', 'romainjeff')->post([
     *     'title'         => 'Test du client API Bitbucket',
     *     'content'       => 'Tout a l\'air de fonctionner correctement',
     *     'priority'      => 'major',
     *     'kind'          => 'bug',
     *     'status'        => 'new',
     *     'responsible'   => 'RomainJeff'
     * ])
     * </code>
     *
     * @param array $params
     * ['title'] = Le titre de l'issue <br>
     * ['content'] = Le contenue de l'issue <br>
     * ['priority'] = La priorité de l'issue (trivial,minor,major,critical,blocker) <br>
     * ['kind'] = Le type de l'issue (bug,enhancement,proposal,task) <br>
     * ['status'] = Le status de l'issue (new,open,resolved,on hold,invalid,duplicate,wontfix) <br>
     * ['responsible'] = La personne a qui sera affectée l'issue (optionnel) <br>
     * ['version'] = La version de l'issue (optionnel)
     * @return string
     */
    public function post(Array $params)
    {
        CURL::setUrl($this->pattern);
        CURL::setMethod('POST');
        CURL::setOpt(CURLOPT_POSTFIELDS, $params);
        CURL::execute();
        CURL::close();
        
        $result = json_decode(CURL::result(), 1);
        
        return $result;
    }
    
    /**
     * Permet de modifier une entrée
     *
     * <code>
     * $client->Issues('bitbucket-client-api', 'romainjeff')->put([
     *     'id'             => 3,
     *     'responsible'    => 'Ho0x'
     * ])
     * </code>
     *
     * @param array $params
     * ['id'] = L'id de l'issue que l'on souhaite modifier <br>
     * ['x']  = Les différents paramètres sont les même que la méthode post()
     * @return string
     */
    public function put(Array $params)
    {
        CURL::setUrl($this->pattern . $params['id']);
        CURL::setMethod('PUT');
        CURL::setOpt(CURLOPT_POSTFIELDS, $params);
        CURL::execute();
        CURL::close();
        
        $result = json_decode(CURL::result(), 1);
        
        return $result;
    }
    
    /**
     * Permet de supprimer une entrée
     *
     * <code>
     * $client->Issues('bitbucket-client-api', 'romainjeff')->delete([
     *     'id' => 3
     * ])
     * </code>
     *
     * @param string $params ['id'] = L'id de l'issue que l'on souhaite supprimer
     * @return string
     */
    public function delete(Array $params)
    {
        CURL::setUrl($this->pattern . $params['id']);
        CURL::setMethod('DELETE');
        CURL::execute();
        CURL::close();
        
        $result = json_decode(CURL::result(), 1);
        
        return $result;
    }
    
}
