<?php
namespace BitbucketApi\Model;

use BitbucketApi\Controller\Curl;

/**
 * Gestion des Repositories
 *
 * @package Bitbucket API
 * @copyright Addweb 2013
 * @author Romain QUILLIOT <romain.addweb@gmail.com>
 */
class Repositories implements Item
{
    private $pattern = "https://bitbucket.org/api/1.0/repositories/{username}/{repository}/";
    
    /**
     * Permet de créer le pattern et initialise cURL
     *
     * @param array $replace
     * @param string $infos
     */
    public function __construct($replace, $infos)
    {
        $this->pattern = str_replace(['{username}','{repository}'], [$replace['account'], $replace['repository']], $this->pattern);
        CURL::init();
        CURL::setID($infos);
    }
    
    /**
     * Recupere les données des repositories
     * 
     * <code>
     * $client->Repositories('xpressimport', 'romainjeff')->get([])
     * </code>
     *
     * @param array $params Tous les repositories
     * @return array
     */
    public function get(Array $params)
    {
        CURL::setUrl($this->pattern);        
        CURL::execute();
        CURL::close();
        
        $result = json_decode(CURL::result(), 1);
        
        return $result;
    }
    
    public function post(Array $params){}
    public function put(Array $params){}
    public function delete(Array $params){}
}