<?php
namespace BitbucketApi\Model;

use BitbucketApi\Controller\Curl;

/**
 * Gestion des Commits
 *
 * @package Bitbucket API
 * @copyright Addweb 2013
 * @author Romain QUILLIOT <romain.addweb@gmail.com>
 */
class Commits implements Item
{
    private $pattern = "https://bitbucket.org/api/2.0/repositories/{username}/{repository}/commits/";
    
    /**
     * Permet de créer le pattern et initialise cURL
     *
     * @param array $replace
     * @param string $infos
     */
    public function __construct($replace, $infos)
    {
        $this->pattern = str_replace(['{username}','{repository}'], [$replace['account'], $replace['repository']], $this->pattern);
        CURL::init();
        CURL::setID($infos);
    }
    
    /**
     * Permet de recuperer les branches
     * 
     * <code>
     * $client->Commits('xpressimport', 'romainjeff')->get([
     *     'revision' => 'dev'
     * ])
     * </code>
     *
     * @param array $params
     * [] = Les commits de toutes les branches
     * ['revision'] = La branche ou le hash du commit
     * @return array
     */
    public function get(Array $params)
    {
        if (!empty($params['revision'])) {
            CURL::setUrl($this->pattern . $params['revision']);
        } else {
            CURL::setUrl($this->pattern);
        }
        
        CURL::execute();
        CURL::close();
        
        $result = json_decode(CURL::result(), 1);
        
        return $result;
    }
    
    public function post(Array $params){}
    public function put(Array $params){}
    public function delete(Array $params){}
    
}

?>