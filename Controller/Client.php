<?php
namespace BitbucketApi\Controller;

/**
 * Classe principale cliente
 *
 * @package Bitbucket API
 * @copyright Addweb 2013
 * @author Romain QUILLIOT <romain.addweb@gmail.com>
 */
class Client
{
    private $username;
    private $password;
    
    /**
     * Prend en paramètre les identifiants ou créer un fichier Config/bitbucket.json
     * contenant les identifiants
     *
     * @param string $username (optionnel)
     * @param string $password (optionnel)
     * @return boolean
     */
	public function __construct($username = null, $password = null) {
        if (empty($username) && empty($password)) {
            $path = dirname(dirname(__FILE__)) ."/Config/bitbucket.json";
            
            if (!file_exists($path)) {
                throw new \Exception("Le fichier de configuration bitbucket.json n'existe pas");
            }
            
            $confFile = json_decode(file_get_contents($path), 1);
            $this->username = $confFile['username'];
            $this->password = $confFile['password'];
            
            return true;
        }
        
		$this->username = $username;
        $this->password = $password;
        return true;
	}
    
    /**
     * Permet d'appeler la bonne classe en les instanciant
     * à la voler
     *
     * @param string $class
     * @param string $repo
     */
    public function __call($class, $repo)
    {
        $account = !empty($repo[1]) ? $repo[1] : $this->username;

        $class = "BitbucketApi\Model\\". $class;
        
        return new $class([
            'repository' => $repo[0],
            'account'    => $account
        ], $this->getID());
    }
    
    /**
     * Permet de recuperer les identifiants de connexion
     *
     * @return array
     */
    private function getID()
    {
        return [
            'username' => $this->username,
            'password' => $this->password
        ];
    }
        
}