<?php
namespace BitbucketApi\Controller;

/**
 * Gestion des fonctions cURL
 *
 * @package Bitbucket API
 * @copyright Addweb 2013
 * @author Romain QUILLIOT <romain.addweb@gmail.com>
 */
class Curl
{
    public static $curl;
    public static $result;
    
    /**
     * Initalise cURL
     */
    public static function init()
    {
        self::$curl = curl_init();
        self::setOpt(CURLOPT_RETURNTRANSFER, true);
    }
    
    /**
     * Paramètre l'url de la requête
     *
     * @param string $url
     */
    public static function setUrl($url)
    {
        self::setOpt(CURLOPT_URL, $url);
    }
    
    /**
     * Paramètre les identifiants de connexion
     *
     * @param array $infos
     */
    public static function setID(Array $infos)
    {
        self::setOpt(CURLOPT_USERPWD, $infos['username'] .":". $infos['password']);
    }
    
    /**
     * Paramètre la méthode de la requête
     *
     * @param string $method
     */
    public static function setMethod($method)
    {
        self::setOpt(CURLOPT_CUSTOMREQUEST, $method);
    }
    
    /**
     * Paramètre une option
     *
     * @param int $opt
     * @param string/int/bool/array $value
     */
    public static function setOpt($opt, $value)
    {
        curl_setopt(self::$curl, intval($opt), $value);
    }
    
    /**
     * Execute la requête cURL
     */
    public static function execute()
    {
        self::$result = curl_exec(self::$curl);
    }
    
    /**
     * Retourne le resultat du serveur distant
     *
     * @return string
     */
    public static function result()
    {
        return self::$result;
    }
    
    /**
     * Ferme la connexion cURL
     */
    public static function close()
    {
        curl_close(self::$curl);
    }
}
